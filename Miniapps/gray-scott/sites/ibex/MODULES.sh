#!/bin/bash
#
# KAUST Visualization Vignettes
#
# Author: James Kress, <james.kress@kaust.edu.sa>
# Copyright KAUST
#

module load cmake/3.24.2/gnu-11.2.1
module load openmpi/4.1.4/gnu11.2.1-cuda11.8
module use /sw/vis2/ibex-gpu.modules/
module load Gray-Scott
