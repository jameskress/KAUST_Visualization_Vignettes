#!/bin/bash
export UNZIP_DISABLE_ZIPBOMB_DETECTION=TRUE
curl -k -O https://download.vis.kaust.edu.sa/pub/data/visualizationVignettes/KAUST_Visualization_Vignettes_Large_Data.zip
unzip KAUST_Visualization_Vignettes_Large_Data.zip
