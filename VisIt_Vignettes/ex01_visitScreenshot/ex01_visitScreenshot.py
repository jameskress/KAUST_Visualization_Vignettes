#
# KAUST Visualization Vignettes
#
# Author: James Kress, <james.kress@kaust.edu.sa>
# Copyright KAUST
#
import os
import sys

print("Running VisIt example script: ", sys.argv[0], "\n")

# Get directory of this script
script_dir = os.path.abspath(os.path.dirname(__file__))
print("Running script from: ", script_dir)

# Open the compute engine if running on cluster
if len(sys.argv) < 4:
    print("Running script locally, not launching a batch job\n")
elif sys.argv[4] == "shaheen":
    OpenComputeEngine(
        "localhost",
        (
            "-l",
            "srun",
            "-p",
            sys.argv[1],
            "-nn",
            sys.argv[2],
            "-np",
            sys.argv[3],
            "-t",
            sys.argv[4],
        ),
    )

elif sys.argv[4] == "ibex":
    OpenComputeEngine(
        "localhost",
        (
            "-l",
            "srun",
            "-p",
            "batch",
            "-nn",
            sys.argv[1],
            "-np",
            sys.argv[2],
            "-t",
            sys.argv[3],
        ),
    )


# Open file and add basic plot
dataFile = script_dir + "/../../data/noise.silo"
OpenDatabase("localhost:" + dataFile, 0)
AddPlot("Pseudocolor", "hardyglobal", 1, 0)
PseudocolorAtts = PseudocolorAttributes()
PseudocolorAtts.colorTableName = "hot_desaturated"
SetPlotOptions(PseudocolorAtts)
DrawPlots()

# Change the annotations on the window
AnnotationAtts = AnnotationAttributes()
AnnotationAtts.userInfoFlag = 0
SetAnnotationAttributes(AnnotationAtts)

SaveWindowAtts = SaveWindowAttributes()
try:
    saveDir = script_dir + "/output"
    os.mkdir(saveDir)
except FileExistsError:
    pass
SaveWindowAtts.outputToCurrentDirectory = 0
SaveWindowAtts.outputDirectory = saveDir
SaveWindowAtts.fileName = "ex01_visit"
SaveWindowAtts.family = 0
SaveWindowAtts.format = (
    SaveWindowAtts.PNG
)  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY, EXR
SaveWindowAtts.width = 2048
SaveWindowAtts.height = 2048
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.saveTiled = 0
SaveWindowAtts.quality = 80
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.NONE  # NONE, PackBits, Jpeg, Deflate, LZW
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = (
    SaveWindowAtts.EqualWidthHeight
)  # NoConstraint, EqualWidthHeight, ScreenProportions
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()

print("\nFinished VisIt example script\n")

# If on Windows wait for user input so that output does not disapear
if os.name == "nt":
    input("Press any key to close")
exit()
