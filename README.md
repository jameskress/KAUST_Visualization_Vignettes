# KAUST Visualization Vignettes

Cookbook of examples for running visualization on KAUST HPC and computing resources. This repository is managed by the KAUST Visualization Core Laboratory. For more information or help contact us using the options below.


## ParaView
KVL supports the use of ParaView on KAUST computing resources. We facilitate the installation of new versions on both Ibex and Shaheen. For more information on ParaView check out the **ParaView_Vignettes** subfolder.


## VisIt
KVL supports the use of VisIt on KAUST computing resources. We facilitate the installation of new versions on both Ibex and Shaheen. For more information on VisIt check out the **VisIt_Vignettes** subfolder.


## In Situ Visualization
KVL supports the implementation of in situ visualization pipelines. We have multiple ways that this can be accomplished depending on user needs. For examples that you can copy and use immediatly for your own projects check out the **In_Situ_Vignettes** subfolder.

## Scripts
Scripts that can help with visualization or movie making outside of ParaView or VisIt.

## For further information
- KVL Wiki: https://wiki.vis.kaust.edu.sa/
- KVL email: help@vis.kaust.edu.sa
