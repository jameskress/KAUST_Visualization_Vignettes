# Scripts
This folder contains scripts that can be useful for visualization or movie making that are not specific to VisIt, ParaView or In Situ.

## createMovieFromImages.sh
1. This script will in general work with any sequence of **png** files.
2. If desired you can change the framerate, encoding, etc. in the script to suit your needs
